#!/bin/bash
# Prompt the user for their Git repository URL
read -p "Enter the URL for your Git repository: " repository_url
# Initialize the fastapiserver folder as a Git repository
if [ "$(pwd)" != "$HOME/fastapiserver" ]; then
  cd fastapiserver
fi
git init
git remote add origin $repository_url
git add .
git commit -m "initial commit on master"
git push origin master
